let lang = "fr"
const cooldown = []
const fs = require('fs')

module.exports = async(client, con, message) => {
    let prefix = client.config.prefix
    let sconfig
    if (message.guild) {
        fs.readdir('./config/', async (err, files) => {
            if (files.includes(message.guild.id+'.json')) {
                sconfig = JSON.parse(fs.readFileSync('./config/'+message.guild.id+'.json', {encoding:'utf-8'}))
                prefix = sconfig.prefix
            } else {
                data = fs.readFileSync('./config/schema.json', {encoding:'utf-8'})
                await fs.writeFileSync('./config/'+message.guild.id+'.json', data, (err) => {
                    if (err) throw err
                })
            }
            if (!Object.values(message.channel.permissionsFor(client.user).serialize())[11]) return
            let command;
            let args = message.content.toLowerCase().split(" ");
            let cmd = message.content.toLowerCase().replace(prefix, "").replace(/[0-9<>@!]/g, "")
            if (!message.guild.me.hasPermission("SEND_MESSAGES")) return;
            if (message.content.startsWith("<@!" + client.user.id + "> ")) {
                cmd = cmd.replace(" ", "")
                args.splice(1, 1)
            }

            if (message.content.startsWith("<@" + client.user.id + "> ")) {
                cmd = cmd.replace(" ", "")
                args.splice(1, 1)
            }

            cmd = cmd.split(" ")[0]

            if (!message.content.toLowerCase().startsWith(prefix) && !message.content.startsWith("<@!" + client.user.id + ">")&& !message.content.startsWith("<@" + client.user.id + ">")) return;

            if (client.commands.has(cmd)) {
                command = client.commands.get(cmd);
            } else command = client.commands.get(client.alias.get(cmd));

            if (command) {
                args.splice(0, 1)
                if (!command.help.dm && message.channel.type === 'dm') return message.channel.send(`:x: La commande \`${command.help.name}\` n'est pas utilisable en mp`)

                if(!client.config.owners.includes(message.author.id)) {

                    if (command.help.ownerOnly) return message.channel.send(`:x: Vous ne pouvez pas utiliser cette commande, elle est réservé aux propriétaires du bot`)
            
                    if (message.channel.type !== 'dm') {
                        if (command.help.modOnly && !message.member.roles.cache.has(sconfig.mod) && !message.member.hasPermission('ADMINISTRATOR')) return message.channel.send(`:x: Pour exécuter cette commande vous devez avoir le rôle mod ou la permission ADMINISTRATEUR`)

                        if (!message.member.hasPermission(command.help.permRequired)) return message.channel.send(`:x: Vous devez avoir la/les permission(s) ${command.help.permRequired.join(', ')} pour utiliser cette commande`)
                
                        if(!message.guild.me.hasPermission(command.help.botPerm)) return message.channel.send(`:x: Pour exécuter cette commande, je dois avoir la/les permission(s) ${command.help.permRequired.join(', ')}`)
                    }
                    if (cooldown.indexOf(message.author.id) > -1) return message.react("⏳").catch(r => console.log("waiting"))
                    cooldown.push(message.author.id)
                    setTimeout(function(){
                        cooldown.splice(cooldown.indexOf(message.author.id), 1);
                    }, 3000)
                }
                command.run(client, message, args, con, lang);

            } else if (message.content.startsWith("<@!" + client.user.id + ">") || message.content.startsWith("<@" + client.user.id + ">")) {
                message.channel.send(" • Mon préfix est `" + prefix + "`.")
            }
        })
    }
}


