const Discord = require('discord.js');
const mysql = require("mysql")
const fs = require("fs")
const { addFunctions } = require('./functions/client')

const client = new Discord.Client({
    ws: { 
        intents: [
            "GUILDS",
            "GUILD_MEMBERS",
            "GUILD_BANS",
            "GUILD_EMOJIS",
            "GUILD_INTEGRATIONS",
            "GUILD_WEBHOOKS",
            "GUILD_INVITES",
            "GUILD_VOICE_STATES",
            "GUILD_PRESENCES",
            "GUILD_MESSAGES",
            "GUILD_MESSAGE_REACTIONS",
            "GUILD_MESSAGE_TYPING",
            "DIRECT_MESSAGES",
            "DIRECT_MESSAGE_REACTIONS",
            "DIRECT_MESSAGE_TYPING"
        ] 
    },
    restTimeOffset: 0,
    disableMentions: 'everyone',
    messageCacheMaxSize: 100
});

addFunctions(client)

client.config = require('./config.json')

const con = mysql.createConnection(client.config.database)

con.connect()
client.commands = new Discord.Collection()
client.alias = new Discord.Collection()

require('events').EventEmitter.prototype._maxListeners = 0;
require('events').EventEmitter.defaultMaxListeners = 0

client.on("ready", async () => {

    let owners = []
    client.config.owners.forEach(owner => {
        client.users.fetch(owner)
        owners.push(client.users.cache.get(owner).tag)
    })

    console.log(
    "───────────── "+client.user.username+" ─────────────\n"+
    "           Name: "+client.user.tag+"            \n"+
    "           ID: " + client.user.id + "       \n"+
    "           Version: 0.0.1       \n"+
    "           Owners: "+owners.join(', ')+"       \n"+
    "           Serveurs:  " + client.guilds.cache.size + "      \n"+
    "           Membres:  " + client.users.cache.size + "      \n"+
    "───────────── "+client.user.username+" ─────────────")

    if (client.config.presence.activated) {
        client.user.setPresence(client.config.presence.list[0]);
        let i = 1
        setInterval(function() {
            client.user.setPresence(client.config.presence.list[i]);
            i ++
            if (i === client.config.presence.list.length) i = 0
        },
        client.config.presence.time)
    }
});

fs.readdir("./commands/", (err, files) => {
    if (err) console.log(err)
    files.forEach(file => {
        if (!file.endsWith(".js")) return console.log("Je n'ai pas pu accepter le fichier " + file + " car il n'est pas en \".js\" .")
        let props = require("./commands/" + file)
        if (!props.help) return console.log("Je n'ai pas pu accepter le fichier " + file + " car il manque la fonction help .")
        client.commands.set(props.help.name, props)
        if (typeof(props.help.alias) === "string") {
            client.alias.set(props.help.alias, props.help.name)
        } else if (typeof(props.help.alias) === "object") {
            for (let i = 0; i < props.help.alias.length; i++) {
                client.alias.set(props.help.alias[i], props.help.name)
            }
        }
    })
})

fs.readdir("./events/", (err, files) => {
    if (err) console.log(err)
    files.forEach(file => {
        if (!file.endsWith(".js")) return console.log("Je n'ai pas pu accepter le fichier " + file + " car il n'est pas en \".js\" .")
        let event = require("./events/" + file)
        client.on(file.split(".")[0], event.bind(null, client, con))
        delete require.cache[require.resolve("./events/" + file)]
    })
})

client.login(client.config.token);